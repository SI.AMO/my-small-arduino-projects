#include <avr/io.h>
#include <string.h>
#include <util/delay.h>
#include "serial.h"

#define BUF_LEN 8

char buf[BUF_LEN];


void start_cap()
{
	int8_t state, new_state; 

	state = 0;
	new_state = 0;

	while(1){
		while( state == new_state ){
			new_state = (PIND & (1<<PIND3)) >> PIND3;
		}

		state = new_state;
		buf[0] = state + '0';

		USART_puts(buf, 1);
	}
}




int main(void)
{
	DDRD &= ~(1 << PIND3);  // set D3 as input.

	USART_puts("ready\r\n", 7);

	while(1)
	{
		USART_gets(buf, BUF_LEN);
			
		if(strncmp(buf, "start", 5) == 0)
			start_cap();
		else USART_puts("noy!\r\n", 6);
	}
}
