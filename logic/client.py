import serial
import time
import threading
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = []
ys = []


# init serial port and bound
# bound rate on two ports must be the same
ser = serial.Serial('/dev/ttyUSB0', 57600, bytesize=serial.EIGHTBITS)

d = ser.readline()
if d != b'ready\r\n':
    exit(1)


# send data via serial port
print("# ", ser.write('start\r\n'.encode()), "was sent.")
# org = time.time()

b = [b'0']

f = open("data.out", "a")
o = time.time()

def t(b, ser, xs, ys):
    while True:
        bb = ser.read(1)
        t1 = time.time()
        print(t1-o, bb.decode('utf-8'))

        f.write(str(t1-o) + " " + b[0].decode() + "\n")
        f.write(str(t1-o) + " " + bb.decode() + "\n")
        f.flush()

        ys.append(int(b[0].decode()))
        ys.append(int(bb.decode()))

        xs.append(t1)
        xs.append(t1)

        b[0] = bb


def animate(i, xs, ys, b):
    xs = xs[-100:]
    ys = ys[-100:]
    ax.clear()
    ax.plot(xs, ys)


def r(ax, xs, yx):
    for line in sys.stdin:
        if line == "r\n":
            print("refresh")
            xs = []
            ys = []
            ax.clear()
            ax.plot(xs, ys)
            f.write("########### new data ############\n")
            f.flush()
        elif line == "q\n":
            exit(0)


x = threading.Thread(target=t, args=(b, ser, xs, ys,))
x.start()
y = threading.Thread(target=r, args=(ax, xs, ys,))
y.start()

ani = animation.FuncAnimation(
        fig, animate, fargs=(xs, ys, b), interval=100
)

plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.1)
plt.autoscale(False)
plt.show()
ser.close()
f.close()
