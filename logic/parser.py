f = open("data.out", "r")
lines = f.readlines()
times, values = [], []


for line in lines:
    t, v = line.split(" ")
    times.append(float(t))
    values.append(v.replace("\n", ""))


distanses = []
for i in range(0, len(times)-2, 4):
    print(times[i], times[i+2])
    distanses.append(abs(times[i]-times[i+2]))


# print(times, values)
# print(min(distanses))
dt = min(distanses)
for i in distanses:
    print(int(i//dt))
